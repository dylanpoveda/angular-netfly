import { Component, OnInit } from '@angular/core';
import {WelcomeDataService} from '../service/data/welcome-data.service'
import {TodoDataService} from '../service/data/todo-data.service'
import { Router } from '@angular/router';
export class Todo{
  constructor(
  public id:number,
	public userName:string,
	public description:string,
	public targetDate:Date,
  public isDone:boolean,
  ){}
}
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(
    private welcomeService:WelcomeDataService,
    private todoService:TodoDataService,
    private router:Router ) { }
  buttonName:string='Invocar'
  sendName=''
  personalGretting
  ngOnInit(): void {
  }
  todos=[]

  handleClick(){
    this.todoService.returnAllTodosService(this.sendName).subscribe(
      response=> {
        this.todos=response
      }
    )
  }
  deleteTodo(username,id){
    this.todoService.deleteTodo(username,id).subscribe(
      response=>this.todoService.returnAllTodosService(this.sendName).subscribe(
        response=> {
          this.todos=response
        }
      )
    )
  }
  addingTodo(){
    this.router.navigate(['new-todo'])
  }
  updateTodo(id){
    this.router.navigate(['todo',id])
  }
}
