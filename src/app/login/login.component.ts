import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HardcodedAuthenticationService} from './../service/hardcoded-authentication.service'
import {BasicAuthenticationService} from './../service/basic-authentication.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  message="Its Works"
  userName=""
  password=""
  errorMessage='Datos Invalidos'
  unValidation=false;
  constructor(
    private router:Router,
    public hardcodedAuthentication : HardcodedAuthenticationService,
    public basicAuthentication:BasicAuthenticationService
    ) {}

  ngOnInit() {
  }
  handleSubmit(){
    if(this.hardcodedAuthentication.authenticate(this.userName,this.password)){
      
      this.router.navigate(['welcome',this.userName])
      this.unValidation=false;
    }else{
      this.unValidation=true;
    } 
  }
  basicAuthenticationLogin(){
      this.basicAuthentication.executeJWTBasicAuthService(this.userName,this.password).subscribe(
        data=>{
          console.log(data)
          this.router.navigate(['welcome',this.userName])
          this.unValidation=false;
        },
        error=>{
            this.unValidation=true;
        }
      )
  }

}
