import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Todo } from '../list/list.component';

@Component({
  selector: 'app-newtodo',
  templateUrl: './newtodo.component.html',
  styleUrls: ['./newtodo.component.css']
})
export class NewtodoComponent implements OnInit {
  todo:Todo
  
  constructor(private serviceData:TodoDataService,
    private router:Router) { }

  ngOnInit(): void {
    this.todo=new Todo(1,'','',new Date(),false)
  }
  createTodo(){
    this.serviceData.addingTodo(this.todo.userName,this.todo).subscribe(
      res=>alert('Se ha creado exitosamente')
    )
  }
  returnPage(){
    this.router.navigate(['/list'])
  }
}
