import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LogoutComponent } from './logout/logout.component';
import {RouteGuardService} from './service/route-guard.service'
import { ListComponent } from './list/list.component';
import { TodoComponent } from './todo/todo.component';
import { NewtodoComponent } from './newtodo/newtodo.component';


const routes: Routes = [
  {path:'welcome/:name', component:WelcomeComponent,canActivate:[RouteGuardService]},
  {path:'list', component:ListComponent,canActivate:[RouteGuardService]},
  {path:'login', component:LoginComponent},
  {path:'logout', component:LogoutComponent,canActivate:[RouteGuardService]},
  {path:'', component:LoginComponent},
  {path:'todo/:id', component:TodoComponent,canActivate:[RouteGuardService]},
  {path:'new-todo', component:NewtodoComponent,canActivate:[RouteGuardService]},
  {path:'**', component:PageNotFoundComponent} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
  