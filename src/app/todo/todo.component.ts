import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Todo } from '../list/list.component';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  id;
  username='test';
  todo:Todo;

  constructor(private serviceData:TodoDataService,
              private router:Router,
              private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.id=this.route.snapshot.params['id']
    this.todo=new Todo(1,'','',new Date(),false)
    this.serviceData.returnTodoByid(this.username, this.id).subscribe(
      response=>this.todo=response
      
    )
  }
  updateTodo(){
    this.serviceData.updateTodo(this.todo.userName,this.todo.id,this.todo).subscribe(
      res=>{
        console.log(res)
        alert("Se ha actualizado"+ this.todo.targetDate)
      }
    )
  }
  returnPage(){
    this.router.navigate(['/list'])
  }
}
