import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HardcodedAuthenticationService {

  constructor() { }

  authenticate(username,password){
    if(username=='Dylan'&&password=='123456'){
      sessionStorage.setItem('authenticateUser',username)
      return true;
    }
    return false;
  }
  isUserLoggin(){
    let user=sessionStorage.getItem('authenticateUser');
    return !(user===null)
  }
  logout(){
    sessionStorage.removeItem('authenticateUser')
  }
}
