import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators';
import { API_URL } from '../constant';

export const TOKEN='token';
export const AUTHENTICATED_USER='authenticateUser'

@Injectable({
  providedIn: 'root'
})
export class BasicAuthenticationService {
  url =`${API_URL}basic-auth`;
  url1=`${API_URL}authenticate`;

  constructor(private http:HttpClient) { }

  executeJWTBasicAuthService(username,password){

    return this.http.post<any>(this.url1,{
      username,
      password
    }).pipe(
      map(
        data=>{
          sessionStorage.setItem(AUTHENTICATED_USER,username)
          sessionStorage.setItem(TOKEN,`Bearer ${data.token}`)
          return data;
        }
      )
    )
  }
  executeBasicAuthService(username,password){

    let basicAuthHeaderString='Basic ' +  window.btoa(username +':'+password);
    let headers=new HttpHeaders({
      Authorization: basicAuthHeaderString
    })
    return this.http.get<AuthenticationBean>(this.url,{headers}).pipe(
      map(
        data=>{
          sessionStorage.setItem(AUTHENTICATED_USER,username)
          sessionStorage.setItem(TOKEN,basicAuthHeaderString  )
          return data;
        }
      )
    )
  }
  getAuthenticatedUser(){
    return sessionStorage.getItem(AUTHENTICATED_USER)
  }
  getAuthenticatedToken(){
    if(this.getAuthenticatedUser())
      return sessionStorage.getItem(TOKEN)
  }
  isUserLoggin(){
    let user=sessionStorage.getItem(AUTHENTICATED_USER);
    return !(user===null)
  }
  logout(){
    sessionStorage.removeItem(AUTHENTICATED_USER)
    sessionStorage.removeItem(TOKEN)
  }
}
export class AuthenticationBean{
  constructor(public message:string){}
}
