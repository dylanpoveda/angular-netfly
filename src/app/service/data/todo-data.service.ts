import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Todo} from '../../../../src/app/list/list.component'
import {API_URL } from '../../constant'
@Injectable({
  providedIn: 'root'
})
export class TodoDataService {

  
  constructor(private http:HttpClient) { }

  returnAllTodosService(username){
    return this.http.get<Todo[]>(`${API_URL}jpa/users/${username}/todos`)
  }
  deleteTodo(username,id){
    return this.http.delete(`${API_URL}jpa/users/${username}/todos/${id}`)
  }
  returnTodoByid(username,id){
    return this.http.get<Todo>(`${API_URL}jpa/users/${username}/todos/${id}`)
  }
  updateTodo(username,id,updatedTodo){
    return this.http.put(`${API_URL}jpa/users/${username}/todos/${id}`,updatedTodo)
  }
  addingTodo(username,todo){
    return this.http.post<Todo>(`${API_URL}jpa/users/${username}`,todo)
  }

}
