import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { API_URL } from 'src/app/constant';
export class WelcomeDataBean {
  constructor(public message:string){}
}
@Injectable({
  providedIn: 'root'
})

export class WelcomeDataService {
   url =`${API_URL}hello-world-bean`;
   url2=`${API_URL}hello-world/path-variable/`


  constructor(
    private http:HttpClient
  ) { }
  executeHelloWorldService(){
    let basicAuthHeaderString=this.createBasicAuthHeader()
    let headers=new HttpHeaders({
      Authorization: basicAuthHeaderString
    })
    return this.http.get<WelcomeDataBean>(this.url,{headers})
  }
  
  returningPersonalGretting(name){
    let basicAuthHeaderString=this.createBasicAuthHeader()
    let headers=new HttpHeaders({
      Authorization: basicAuthHeaderString
    })
    return this.http.get<WelcomeDataBean>(this.url2 + ''+ name,{headers})
  }
  createBasicAuthHeader(){
    let userName='dylan'
    let password='123456'
    let basicAuthHeaderString='Basic ' +  window.btoa(userName +':'+password);
    return basicAuthHeaderString;
  }
}
